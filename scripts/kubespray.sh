#!/bin/bash
set -eu

cd ../hackathon

# customer prefix and api key
export $(grep res_prefix terraform.tfvars |tr -d "[:blank:]"|sed 's/#.*$//')
export $(grep api_key terraform.tfvars |tr -d "[:blank:]"|sed -e 's/#.*$//' -e 's/"//g')

## activate service account
gcloud auth activate-service-account --key-file ${api_key}

# Create new rsa keys
if [ ! -f cust_id_rsa ] ; then 
   ssh-keygen -t rsa -b 2048 -f cust_id_rsa -q -N ""
fi

# Load keys
eval $(ssh-agent)
ssh-add -D
ssh-add cust_id_rsa


# Create infrastructure in gcp
terraform init
terraform plan -out cust.plan
terraform apply cust.plan
