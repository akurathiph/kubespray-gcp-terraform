pub_key = "cust_id_rsa.pub" # path for a key to login to jumpbox
api_key = "api_key.json" # GCP credentials file
region = "us-east1"
zone = "us-east1-b"
project = "project-id" #CHANGEME
res_prefix = "kubern" #CHANGEME
subnet_cidr = "10.0.0.0/16"
master_group_size = 1
cluster_zones = ["us-east1-b", "us-east1-c", "us-east1-d"]
jumpbox_create = false
jumpbox_type = "n1-standard-1"
jumpbox_image = "ubuntu-os-cloud/ubuntu-1604-lts"
master_type = "n1-standard-1"
os_image = "ubuntu-os-cloud/ubuntu-1604-lts"
owner = "a_phanimca"
worker_type = "n1-standard-1"
worker_group_size = 2
update_strategy = "MIGRATE"

service_account_scopes = [
  "https://www.googleapis.com/auth/devstorage.read_only",
  "https://www.googleapis.com/auth/logging.write",
  "https://www.googleapis.com/auth/trace.append",
  "https://www.googleapis.com/auth/compute",
  "https://www.googleapis.com/auth/monitoring",
]